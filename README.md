OgreOggSound
===

A wrapper around OpenAL to quickly add 2D/3D audio to applications.
It supports OGG/WAV formats and is designed to integrate into OGRE applications.
It has Static and multi-threaded stream support using BOOST / POCO threads.
